# Interlink Desktop Client

The cross-platform desktop application for connecting to the Honeydew Interlink Server.

## Getting Started

This application is powered by Electron and uses various Electron build scripts. While all of those
should automatically install with ```npm install``` it is possible that a global install of Electron
is needed for building this (not tested).

### Prerequisites

Depending on the target build platform, there are a lot of local credentials needed for building. Follow the 
guides on Electron's website for more details.

One big gotcha' is that the build script expects a Mac_Distribution.provisionprofile to exist in the root
directory when building for Mac.

### Installing

Ensure that all dependencies are needed for this project:

```bash
$ npm install
```

Styling is handled with SASS, so any changes made to the styling will require re-running the Gulp build:

```bash
$ gulp sass #For single build
$ gulp sass:watch #For development, will recompile on changes
```

### Platform-Specific Builds

#### MacOS

The Electron guides cover the needed setup for building for MacOS, but the important steps are as follows.

* Sign up for an Apple Developer account
* Generate the needed credentials and app ids
* Create and download provision profiles
* Run build scripts

This has already been done, so doing it again shouldn't be necessary unless an entire
new version is being created that can't reuse these credentials, for some reason.


## Built With

* [Electron](https://electronjs.org/) - Build cross platform desktop apps with JavaScript, HTML, and CSS

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Scott Peterson** - *Initial work* - [Scottomaton](https://gitlab.com/scottomaton)
