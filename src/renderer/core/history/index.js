import angular from 'angular';

import historyTemplate from './history.html';
import historyController from './history.controller';
import historyService from './history.service';

const moduleName = 'historyModule';

angular.module(moduleName, [])
  .config(historyRoutes)
  .service('folderHistory', historyService);

function historyRoutes($routeProvider) {
  'use strict';
  'ngInject';

  $routeProvider.when('/', {
    controller : historyController,
    controllerAs : 'vm',
    template : historyTemplate
  });
}

historyRoutes.$inject = ['$routeProvider'];

export default moduleName;