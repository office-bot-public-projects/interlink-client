import { join, basename } from 'path';

function historyService($timeout) {
  'use strict';
  'ngInject';

  let _recentList = [];

  this.addToList = addToList;
  this.getList = getList;

  function addToList(pathInfo) {
    let mountPoint = localStorage.getItem('mountPoint');
    $timeout(() => {
      _recentList.push({
        path : pathInfo.path,
        name : basename(pathInfo.path),
        success : pathInfo.success,
        timestamp : new Date()
      });
    },0);
  }

  function getList() {
    return _recentList;
  }
}

historyService.$inject = ['$timeout'];

export default historyService;