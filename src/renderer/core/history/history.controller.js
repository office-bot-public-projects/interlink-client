function HistoryController(folderHistory, connection) {
  'use strict';
  'ngInject';

  let vm = this;
  Object.assign(vm, {
    openFolder 
  });

  vm.history = folderHistory.getList;

  return vm;

  function openFolder(pathInfo) {
    if (!pathInfo || !pathInfo.path) {
      throw new Error('Missing path info');
      return;
    }
    
    connection.doOpenFolder(pathInfo.path, err => {
      if (err) {
        console.error(err);
      }
    });
  }
}

HistoryController.$inject = ['folderHistory', 'connection'];

export default HistoryController;