import angular from 'angular';
import connectionFactory from './connection.factory';
import connectionDirective from './connection-status.directive';

const moduleName = 'connectionModule';

angular.module(moduleName, [])
  .directive('connectionStatus', connectionDirective)
  .factory('connection', connectionFactory);

export default moduleName;