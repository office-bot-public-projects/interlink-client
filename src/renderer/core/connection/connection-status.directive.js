function ConnectionStatusDirective() {
  'use strict';

  let directive = {
    restrict : 'E',
    template : `
      <span>
        <a href='#!/settings' ng-show='isConnected()'>Connected</span>
        <a href='#!/settings' ng-hide='isConnected()' class='text-danger'>Not Connected</a>.
      </span>
    `,
    controller : ['$scope', 'connection', controllerFn]
  };
  return directive;

  function controllerFn($scope, connection) {
    $scope.isConnected = connection.isConnected;
  }
}

export default  ConnectionStatusDirective;