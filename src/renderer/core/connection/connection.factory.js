import path from 'path';
import fs from 'fs';
import os from 'os';
import crypto from 'crypto';
import {spawnSync} from 'child_process';
import io from 'socket.io-client';

function connectionFactory($q, folderHistory, $timeout) {
  'use strict';
  'ngInject';

  let socket;

  let mountPoint;
  let socketUri;

  let _connectionStatus = false;

  function init() {
    let savedMountPoint = localStorage.getItem('mountPoint');
    let savedSocketURI = localStorage.getItem('socketURI');

    if (savedMountPoint && savedSocketURI) {        
      setSocketURI(savedSocketURI);
      setMountPath(savedMountPoint);
    }
  }

  function isConnected() {
    return _connectionStatus;
  }

  function setConnectionStatus(newStatus) {
    $timeout(() => {
      _connectionStatus = newStatus;
    },0);
  }

  function searchFolders(keyword) {
    return $q((resolve, reject) => {
      if (!socket) {
        return reject(new Error('Socket not connected.'));
      }
      let clientKey = localStorage.getItem('clientKey');
      let signature = crypto.createHmac('sha1', clientKey).update(keyword).digest('hex');

      socket.emit('resources.folders:search', {keyword : keyword, signature : signature}, function(err, response) {
        if (err) {
          return reject(new Error(err));
        }
        return resolve(response);
      });
    });
  }

  function setSocketURI(newUri) {
    return $q((resolve, reject) => {
      if (socket) {
        socket.disconnect();
      }
      socket = io.connect(newUri);
      socket.on('connect_error', (err) => {
        setConnectionStatus(false);
        socket.disconnect();
        reject(new Error('Could not connect to remote server.'))
      });
      socket.on('disconnect', () => {
        setConnectionStatus(false);
      });
      //This one doesn't appear to fire
      socket.on('reconnect', () => {
        setConnectionStatus(true);
      });
      socket.on('connect', () => {
        setConnectionStatus(true);
        setHandshake();
        registerListeners();
        return resolve();
      });
    });
  }

  function setHandshake() {
    socket.on('identify-request', function() {
      let clientKey = localStorage.getItem('clientKey');
      socket.emit('identify-response', { key : clientKey});
    })
  }

  function registerListeners() {
    if (!socket) {
      return;
    }
    socket.on('fs:open-folder', (data, callback) => {
      if ('function' !== typeof callback) {
        callback = function() {};
      }
      if (!data || !data.path || !data.signature) {
        return callback('Invalid interlink request payload.', false);
      }
      let clientKey = localStorage.getItem('clientKey');
      let signature = crypto.createHmac('sha1', clientKey).update(data.path).digest('hex');

      if (data.signature.length && data.signature === signature) {
        doOpenFolder(data.path, (err) => {          
          if (err) {
            let msg = '';
            if ('function' === typeof err.message) {
              msg = err.message;
            } else {
              msg = err.toString();
            }
            callback(msg, false);
            folderHistory.addToList({
              path : data.path,
              success : false
            });
          } else {
            callback(null, true);
            folderHistory.addToList({
              path : data.path,
              success : true
            });
          }
        });
      } else {
        return callback('Invalid interlink request signature.', false);
      }
    });
  }

  function setMountPath(newPath) {
    return $q((resolve, reject) => {
      let isReachable = checkIsReachable(newPath);
      if (!isReachable) {
        return reject(new Error('Unable to reach mount point.'));
      }
      mountPoint = newPath;
      return resolve();
    });
  }

  function doOpenFolder(relativePath, cb) {
    if ('function' !== typeof cb) {
      cb = function () {};
    }
    var p = os.platform();
    switch (p) {
      case 'darwin' :
        openFolderMac(relativePath, cb);
        break;
      case 'win32' :
        openFolderWin32(relativePath, cb)
        break;
    }
  }

  function openFolderMac(relativePath, cb) {
    if ('function' !== typeof cb) {
      cb = function() {};
    }
    var absolutePath = resolveMac(relativePath);
    if (!absolutePath) {
      return cb(new Error(`Could not locate ${relativePath}.`));
    }
    try {
      spawnSync('open',[absolutePath]);
      cb();
    } catch(e) {
      cb(e);
    }
  }

  function openFolderWin32(relativePath, cb) {
    if ('function' !== typeof cb) {
      cb = function() {};
    }
    var absolutePath = resolveWin32(relativePath);
    try {
      spawnSync('explorer',[absolutePath]);
      cb();
    } catch(e) {
      cb(e);
    }
  }

  function resolveMac(relativePath) {
    var mountPath = localStorage.getItem('mountPoint');
    var folderPath = path.join(mountPath, relativePath);
    if (!checkIsReachable(folderPath)) {
      return false;
    }
    if (!fs.lstatSync(folderPath).isDirectory()) {
      return false;
    }
    return folderPath;
  }

  function resolveWin32(relativePath) {
    var mountPath = localStorage.getItem('mountPoint');
    var folderPath = path.join(mountPath, relativePath);
    if (!checkIsReachable(folderPath)) {
      return false;
    }
    if (!fs.lstatSync(folderPath).isDirectory()) {
      return false;
    }
    return folderPath;
  }
  
  function checkIsReachable(mountPath) {
    let hasAccess = false;
    try {
      fs.accessSync(mountPath, fs.constants.R_OK | fs.constants.W_OK);
      hasAccess = true;
    } catch (err) {
      hasAccess = false;
    }
    return hasAccess;
  }

  return {
    setMountPath,
    setSocketURI,
    isConnected,
    doOpenFolder,
    searchFolders,
    init
  }
}

connectionFactory.$inject = ['$q', 'folderHistory', '$timeout'];

export default connectionFactory;