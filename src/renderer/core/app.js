import angular from 'angular';

const moduleName = 'InterlinkClient';
import router from 'angular-route';
import application from './application';
import connection from './connection';
import history from './history';
import mainNavigation from './main-navigation';
import popupStatus from './popup-status';
import scrollPane from './scroll-pane';
import search from './search';
import selectMountPoint from './select-mount-point';
import settings from './settings';

angular.module(moduleName, [
  router,
  application,
  connection,
  history,
  mainNavigation,
  popupStatus,
  scrollPane,
  search,
  selectMountPoint,
  settings
]);

export default moduleName;