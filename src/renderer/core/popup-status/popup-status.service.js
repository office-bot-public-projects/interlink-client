function popupStatusService($rootScope) {
  'use strict';
  'ngInject';
  
  this.addMessage = function(msg, ttl) {
    $rootScope.$emit('popup-status:add-message', {
      message : msg,
      ttl : ttl
    });
  }
}

popupStatusService.$inject = ['$rootScope'];

export default popupStatusService;