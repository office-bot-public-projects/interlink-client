function popupStatusDirective() {
  'use strict';

  let directive = {
    restrict : 'E',
    template : `
      <section id='popup-status' ng-class='{"has-message" : message}'>
        <p>{{message}}</p>
      </section>
    `,
    controller : ['$scope','$timeout','$rootScope',controllerFn]
  }

  return directive;

  function controllerFn($scope, $timeout, $rootScope) {
    'ngInject';

    $rootScope.$on('popup-status:add-message', (event, data) => {
      if (data && data.message && data.message.length) {
        $scope.message = data.message;

        let ttl = 2000;
        if ('number' === typeof data.ttl) {
          ttl = data.ttl;
        }
        $timeout(() => {
          $scope.message = null;
        }, ttl);
      }
    });
  }

}

export default popupStatusDirective;