import angular from 'angular';
import popupDirective from './popup-status.directive';
import popupService from './popup-status.service';

const moduleName = 'popupStatusModule';

angular.module(moduleName, [])
  .directive('popupStatus', popupDirective)
  .service('popupMessage', popupService);

export default moduleName;