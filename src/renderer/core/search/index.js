import angular from 'angular';
import searchController from './search.controller';
import searchTemplate from './search.html';

const moduleName = 'searchModule';

angular.module(moduleName, [])
  .config(searchRoutes);

function searchRoutes($routeProvider) {
  'use strict';
  'ngInject';

  $routeProvider.when('/search', {
    controller : searchController,
    controllerAs : 'vm',
    template : searchTemplate
  });
}

searchRoutes.$inject = ['$routeProvider'];

export default moduleName;