import path from 'path';

function SearchController(connection, folderHistory, popupMessage) {
  'use strict';
  'ngInject';


  let vm = this;
  Object.assign(vm, {
    runSearch,
    openFolder
  });

  return vm;

  function runSearch() {
    connection.searchFolders(vm.keyword).then(results => {
      let cleanedResults = results.map(item => {
        return {
          path : item.relativePath,
          name : path.basename(item.relativePath)
        };
      });
      vm.results = cleanedResults;
    }).catch(err => {
      vm.results = [];
    });

  }

  function openFolder(pathInfo) {
    connection.doOpenFolder(pathInfo.path, err => {
      if (err) {
        folderHistory.addToList({
          path : pathInfo.path,
          success : false
        });
        let name = path.basename(pathInfo.path);
        popupMessage.addMessage(`Could not open '${name}'.`);
      } else {
        folderHistory.addToList({
          path : pathInfo.path,
          success : true
        });
      }
      //otherwise, add to recent list
    });
  }
}

SearchController.$inject = ['connection', 'folderHistory', 'popupMessage'];

export default SearchController;