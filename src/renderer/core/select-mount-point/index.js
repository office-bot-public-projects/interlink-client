import angular from 'angular';
import selectMountDirective from './select-mount-point.directive';

const moduleName = 'selectMountPointModule';

angular.module(moduleName, [])
  .directive('selectMountPoint', selectMountDirective);

export default moduleName;