import $ from 'jquery';

function selectMountPointDirective() {
  'use strict';

  let directive = {
    restrict : 'E',
    scope : {
      onSelect : '='
    },
    template : `
      <span>
        <button class='btn btn-outline-secondary btn-block' ng-click='triggerSelect()'>
          Select Folder
        </button>
        <input type="file" webkitdirectory style='display: none' />
      </span>
    `,
    link : linkFn
  };

  return directive;

  function linkFn(scope, elem, attrs) {
    
    let input = $(elem).find('input[type="file"]');

    scope.triggerSelect = function() {
      input[0].click();
    }

    input.on('change', function() {
      scope.$apply(() => {
        scope.onSelect.call(this, input[0].files[0].path);
      })
    });
  }
}

export default selectMountPointDirective;