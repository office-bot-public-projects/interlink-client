import angular from 'angular';
import scrollPaneDirective from './scroll-pane.directive';

const moduleName = 'scrollPaneModule';

angular.module(moduleName, [])
  .directive('scrollPane', scrollPaneDirective);

export default moduleName;