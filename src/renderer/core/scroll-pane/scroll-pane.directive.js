function scrollPaneDirective() {
  'use strict';

  let directive = {
    restrict : 'E',
    replace : true,
    transclude : true,
    template : `<div class='_scrollPane' ng-transclude></div>`
  };

  return directive;
}

export default scrollPaneDirective;