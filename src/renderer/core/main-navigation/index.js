import angular from 'angular';
import navDirective from './main-nav.directive';

const moduleName = 'mainNavigationModule';

angular.module(moduleName, [])
  .directive('mainNav', navDirective);

export default moduleName;