import $ from 'jquery';

/**
 * This directive simply toggles the active state
 * for the header navigation. When active, the links 
 * will have a small arrow beneath them (from css)
 */
function MainNavDirective($rootScope, $location) {
  'use strict';
  'ngInject';

  let directive = {
    restrict : 'A',
    link : linkFn
  };

  return directive;

  /**
   * Link function for directive
   * @param {Scope} scope 
   * @param {DOMElement} elem 
   * @param {object} attrs 
   */
  function linkFn(scope, elem, attrs) {
    $rootScope.$on('$routeChangeSuccess', () => {
      let hashPrefix = '#!';
      let pathMatch = `${hashPrefix}${$location.path()}`;

      $(elem).find('a').removeClass('active');
      $(elem).find('a[href="'+pathMatch+'"]').addClass('active');

    });

  }
}

MainNavDirective.$inject = ['$rootScope', '$location'];

export default MainNavDirective;