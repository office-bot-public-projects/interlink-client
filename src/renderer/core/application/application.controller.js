function ApplicationController(connection) {
  'use strict';
  'ngInject';

  connection.init();

}

ApplicationController.$inject = ['connection'];

export default ApplicationController;