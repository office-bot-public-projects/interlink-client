import angular from 'angular';
import electron from 'electron';
import applicationController from './application.controller';

const moduleName = 'applicationModule';
const app = electron.remote.app;

/**
 * An important note! The app version is wrong when in dev mode -
 * it will only return the electron version. This is because of some
 * setup deep within electron-webpack
 */
angular.module(moduleName, [])
  .constant('APP_CONFIG',{
    "VERSION" : app.getVersion(),
    "NAME" : app.getName()
  })
  .controller('ApplicationController', applicationController);


export default moduleName;