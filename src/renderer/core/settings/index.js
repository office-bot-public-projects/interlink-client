import angular from 'angular';
import settingsController from './settings.controller';
import settingsTemplate from './settings.html';

const moduleName = 'settingsModule';

angular.module(moduleName, [])
  .config(settingsRoutes);

function settingsRoutes($routeProvider) {
  'use strict';
  'ngInject';

  $routeProvider.when('/settings', {
    controller : settingsController,
    controllerAs : 'vm',
    template : settingsTemplate
  });
}

settingsRoutes.$inject = ['$routeProvider'];

export default moduleName;