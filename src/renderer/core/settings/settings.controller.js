import crypto from 'crypto';

function SettingsController(connection, APP_CONFIG) {
  'use strict';
  'ngInject';

  const KEY_STORAGE_PATH = 'clientKey'; //Used to get/set localstorage
  const SOCKET_STORAGE_PATH = 'socketURI';
  const MOUNT_STORAGE_PATH = 'mountPoint';

  let vm = this;

  Object.assign(vm, {
    saveSocketUrl,
    saveMountPoint,
    setMountPoint,
    connect
  });

  init();

  return vm;

  function saveSocketUrl() {
    localStorage.setItem(SOCKET_STORAGE_PATH, vm.socketUrl);
  }

  function getSocketUrl() {
    return localStorage.getItem(SOCKET_STORAGE_PATH);
  }

  function getMountPoint() {
    return localStorage.getItem(MOUNT_STORAGE_PATH);
  }

  function setMountPoint(newMountPoint) {
    vm.fsMount = newMountPoint;
    return saveMountPoint();
  }

  function saveMountPoint() {
    if ('string' === typeof vm.fsMount && vm.fsMount.length) {
      localStorage.setItem(MOUNT_STORAGE_PATH, vm.fsMount);
    } else {
      localStorage.removeItem(MOUNT_STORAGE_PATH);
    }
  }

  function connect() {
    connection.setSocketURI(vm.socketUrl).then(() => {
      vm.invalidSocket = false;
    }).catch(err => {
      vm.invalidSocket = true;
    });
    connection.setMountPath(vm.fsMount).then(() => {
      vm.invalidMount = false;
    }).catch(err => {
      vm.invalidMount = true;
    })
  }

  function init() {
    vm.appVersion = APP_CONFIG.VERSION;

    vm.pairingKey = getPairingKey();
    vm.socketUrl = getSocketUrl();
    vm.fsMount = getMountPoint();
  }

  function getPairingKey() {
    let key = localStorage.getItem(KEY_STORAGE_PATH);
    if (!key) {
      key = generatePairingKey();
      setPairingKey(key);
    }
    return key;
  }

  function setPairingKey(newKey) {
    localStorage.setItem(KEY_STORAGE_PATH, newKey);
  }

  function generatePairingKey() {
    return crypto.randomBytes(5).toString('hex');
  }
}

SettingsController.$inject = ['connection', 'APP_CONFIG'];

export default SettingsController;