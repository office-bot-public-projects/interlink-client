'use strict'

import { app, BrowserWindow, Menu } from 'electron';
import * as path from 'path';
import { format as formatUrl } from 'url';
import { autoUpdater } from 'electron-updater';
import * as log from 'electron-log';

// Configure some basic logging for the updater
autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';


const isDevelopment = process.env.NODE_ENV !== 'production'

// global reference to mainWindow (necessary to prevent window from being garbage collected)
let mainWindow;
let hasKnownUpdate = false;

function createMainWindow() {
  const window = new BrowserWindow({
      width: 370,
      height: 550,
      webPreferences: {
        nodeIntegration: true,
        preload: path.join(__dirname, '/preload.js')
      }
    });

  if (isDevelopment) {
    window.webContents.openDevTools()
  }

  if (isDevelopment) {
    window.loadURL(`http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`)
  }
  else {
    window.loadURL(formatUrl({
      pathname: path.join(__dirname, 'index.html'),
      protocol: 'file',
      slashes: true
    }))
  }

  window.on('closed', () => {
    mainWindow = null
  })

  window.webContents.on('devtools-opened', () => {
    window.focus()
    setImmediate(() => {
      window.focus()
    })
  });

  makeMenu();

  return window
}
//Only check on startup for now
autoUpdater.checkForUpdatesAndNotify();

//Trying to get Windows auto updater to work with these two lines:
if (process.platform === 'win32') {
  app.setAppUserModelId('com.office-bot.interlink-client');
  app.setAsDefaultProtocolClient('interlink');
}

// quit application when all windows are closed
app.on('window-all-closed', () => {
  // on macOS it is common for applications to stay open until the user explicitly quits
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // on macOS it is common to re-create a window even after all windows have been closed
  if (mainWindow === null) {
    mainWindow = createMainWindow()
  }
})

// create main BrowserWindow when electron is ready
app.on('ready', () => {
  mainWindow = createMainWindow()
})

function makeMacMenu() {
  let template = [
    { label : 'Interlink',
      submenu : [
        { label : 'About Interlink Client', role : 'about' },
        { type: 'separator' },
        { role : 'quit', label : 'Quit Interlink Client'}
      ]
    },
    {
      label: 'Window',
      submenu: [
        { role: 'minimize' },
        { role: 'zoom' },
        { type: 'separator' },
        { role: 'front' },
        { type: 'separator' },
        { role: 'close' }
      ]
    },
  ];
  return template;
}

function makeWinMenu() {
  let template = [
    { label : 'File',
      submenu : [{
        role : 'quit', label : 'Exit'
      }]
    }
  ];
  return template;
}

function makeMenu() {
  let template;
  if (process.platform === 'darwin') {
    template = makeMacMenu();
  } else if (process.platform === 'win32') {
    template = makeWinMenu();
  } else {
    return;
  }
  const menu = Menu.buildFromTemplate(template)
  Menu.setApplicationMenu(menu)
}