const { remote } = require('electron');

if (process.platform == 'darwin') {

  const { systemPreferences } = remote

  const setOSTheme = () => {
    let theme = systemPreferences.isDarkMode() ? 'dark' : 'light'
    window.localStorage.os_theme = theme
    //
    // Defined in index.html, so undefined when launching the app.
    // Will be defined for `systemPreferences.subscribeNotification` callback.
    //
    if ('__setTheme' in window) {
      window.__setTheme()
    }
  }

  systemPreferences.subscribeNotification(
    'AppleInterfaceThemeChangedNotification',
    setOSTheme,
  )

  setOSTheme();
}

if (process.platform == 'win32') {
  const { systemPreferences } = remote
  const {exec} = require('child_process');

  const setOSTheme = () => {
    // console.log("called");
    // console.log(systemPreferences.isInvertedColorScheme());
    let keyPath = 'HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize';
    exec(`reg query ${keyPath} /v AppsUseLightTheme`, (error, stdout, stderr) => {
      if (error) {
        return;
      }
      if (stdout && 'function' === typeof stdout.toString) {
        let matcher = stdout.match(/AppsUseLightTheme\s+REG_DWORD\s+0x([0-1])/);
        if (!matcher || matcher.length !== 2) {
          return;
        }
        //we want the first capture group, ie the value within "()", which
        //is at index 1
        let theme = matcher[1] === "1" ? 'light' : 'dark'; //Should be "1" or "0"
        window.localStorage.os_theme = theme
        if ('__setTheme' in window) {
          window.__setTheme()
        }
      }
    });
        
  } 

  // This is probably terrible for performance...
  setInterval(() => {
    setOSTheme();
  }, 2000);

  // Doesn't work?
  // systemPreferences.on('color-changed', () => {
  //   setOSTheme();
  // });

  // This doens't give us the dark theme, just the high contrast inverted
  // systemPreferences.on('inverted-color-scheme-changed', () => {
  //   setOSTheme();
  // });

  setOSTheme();

}